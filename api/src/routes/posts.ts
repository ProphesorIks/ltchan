import express from 'express'
import { UploadedFile } from 'express-fileupload'
import Post from '../models/Post'

const router = express.Router()
let id = 0

router.get('/', async (req, resp) => {
  const posts = await Post.find()
  resp.json(posts)
})

router.get('/specific', (req, resp) => {
  resp.send('Specific post')
})

router.post('/', (req, resp) => {
  const post = new Post({
    id: id,
    author: req.body.author,
    content: req.body.content
  })

  let fileError = false
  if (req.files && req.files['files[]']) {
    const file: UploadedFile[] = <UploadedFile[]> req.files['files[]']
    file.every(element => {
      let uploaded = true
      element.mv('./uploads/' + element.name, (err) => {
        if (err) {
          resp.status(400).json({ error: err })
          fileError = true
          uploaded = false
        }
      })

      return uploaded
    })
  }

  if (!fileError) {
    post.save()
      .then((data: string) => {
        ++id
        resp.status(200).json(data)
      })
      .catch((error: unknown) => {
        resp.status(400).json({ message: error })
      })
  }
})

router.get('/:postId', (req, resp) => {
  Post.findById(req.params.postId)
    .then((data: string) => {
      resp.status(200).json(data)
    })
    .catch((error: unknown) => {
      console.log(error)
      resp.status(400).json(error)
    })
})

export default router
