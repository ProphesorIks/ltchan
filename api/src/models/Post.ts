import mongoose from 'mongoose'

const PostSchema = new mongoose.Schema({
  id: {
    type: Number,
    required: true
  },
  board: {
    type: mongoose.Types.ObjectId,
    required: false
  },
  author: {
    type: String,
    default: 'Anonymous'
  },
  content: {
    type: String,
    required: true
  },
  files: [String],
  date: {
    type: Date,
    default: Date.now
  }
})

export default mongoose.model('Posts', PostSchema)
