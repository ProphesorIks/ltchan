import 'dotenv/config'
import express from 'express'
import mongoose from 'mongoose'
import postsRouter from './routes/posts'
import fileUpload from 'express-fileupload'

mongoose.connect(process.env.DB_CONNECTION || '', () => {
  console.log('conneced to db!')
})

const app = express()

app.use(express.json({ type: 'application/*+json' }))
app.use(express.urlencoded({ extended: true }))
app.use(fileUpload())

app.get('/', (req, res) => {
  res.send('Hello world!')
})

app.use('/posts', postsRouter)
app.listen(3000, () => console.log('Server running'))
