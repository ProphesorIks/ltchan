export enum FileFormat {
    Image,
    Text,
    Video,
    Audio,
    Binary,
    Signature
}
export type FileType = {
    uuid: string,
    postId: number,
    path: string,
    type: FileFormat,
    originalName: string
}
