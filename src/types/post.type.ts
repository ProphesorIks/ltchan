import type { FileType } from './file.type'

export type PostType = {
    uuid: string,
    id: number,
    board: string,
    author: string,
    replyPostNumbers: number[],
    content: string,
    files: FileType[]
    date: Date;
}
