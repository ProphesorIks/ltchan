import { writable } from 'svelte/store'

export const PostStore = writable([
  {
    uuid: '2de6efbc-f175-48b4-8466-4217d4bd7b04',
    id: 114,
    board: 'g',
    author: 'asdfsdafasdf',
    replyPostNumbers: [118],
    content: 'Testas testauskas',
    files: [],
    date: new Date(2021, 12, 25)
  },
  {
    uuid: '3e3ede05-9c5f-425e-9567-d5d8704c5009',
    id: 118,
    board: 'g',
    author: 'asdfs',
    replyPostNumbers: [200],
    content: 'Testas testuliukas',
    files: [],
    date: new Date(2021, 12, 26)
  },
  {
    uuid: 'c05aeb1a-b63a-431e-8540-50bc2665b4c7',
    id: 119,
    board: 'g',
    author: 'asdfsdafasdf',
    replyPostNumbers: [],
    content: 'Rabarbaras',
    files: [],
    date: new Date(2021, 12, 27)
  },
  {
    uuid: 'db27e520-a805-4688-9aa5-c4afa2ab21ff',
    id: 200,
    board: 'g',
    author: 'asdfsdafasdf',
    replyPostNumbers: [],
    content: 'Rokas burokas',
    files: [],
    date: new Date(2021, 12, 25)
  }

])
